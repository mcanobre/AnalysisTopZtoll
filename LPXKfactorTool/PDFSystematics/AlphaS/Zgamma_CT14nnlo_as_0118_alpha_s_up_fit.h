double Zgamma_CT14nnlo_as_0118_alpha_s_up_fit(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 6.68344, 5.71411, 4.5364, 3.92704, 3.5363,
                        3.24618, 3.03919, 2.891, 2.78144, 2.66378,
                        2.5613, 2.31077, 2.11987, 1.97224, 1.83434,
                        1.61626, 1.42569, 1.1225, 0.852309, 0.610022,
                        0.395145, 0.192575, 0.0288745, 0.21294, 0.604934,
                        0.942876, 1.23934, 1.51604, 1.76703, 1.9943,
                        2.20451, 2.38231, 2.52101, 2.63011, 2.6926,
                        2.71481, 2.69206, 2.63219, 2.38515, 1.99247,
                        1.47279, 1.53493, 2.55593, 3.52601, 4.3358,
                        4.82307, 4.77765, 3.86508, 1.87319 };
   const double fB[49] = { -0.29577, -0.193996, -0.0734154, -0.0484626, -0.032764,
                        -0.024741, -0.0174075, -0.0121816, -0.0111905, -0.0112224,
                        -0.00996111, -0.00913892, -0.00645516, -0.00566472, -0.00514906,
                        -0.00395572, -0.00354728, -0.00276859, -0.00257982, -0.00228639,
                        -0.00198955, -0.00227883, 0.000116746, 0.00242281, 0.0012573,
                        0.00130724, 0.00112667, 0.00106411, 0.000949183, 0.000878249,
                        0.000787511, 0.000627757, 0.000499464, 0.000347991, 0.00016765,
                        -2.18918e-06, -0.00016531, -0.000327995, -0.000620638, -0.00102775,
                        -0.00074252, 0.00125257, 0.00223105, 0.0017697, 0.00136939,
                        0.000535116, -0.00085877, -0.00284799, -0.00517601 };
   const double fC[49] = { 0.014634, 0.0108096, 0.00124845, 0.00124684, 0.00032302,
                        0.00047928, 0.000254064, 0.000268529, -0.000169416, 0.000166225,
                        -4.00999e-05, 7.29878e-05, 3.43624e-05, -2.74475e-06, 2.33712e-05,
                        4.95619e-07, 7.67322e-06, 1.13682e-07, 1.77399e-06, 1.16032e-06,
                        1.80806e-06, -4.70088e-06, 2.86567e-05, -5.59597e-06, 9.33893e-07,
                        -7.34126e-07, 1.1853e-08, -2.62094e-07, -1.97606e-07, -8.61315e-08,
                        -2.76819e-07, -3.62198e-07, -1.50973e-07, -4.54918e-07, -2.66446e-07,
                        -4.12911e-07, -2.39571e-07, -4.11172e-07, -1.74113e-07, -6.4011e-07,
                        1.21057e-06, 2.77962e-06, -8.22671e-07, -1.00017e-07, -7.00607e-07,
                        -9.67943e-07, -1.81983e-06, -2.15862e-06, 500 };
   const double fD[49] = { -0.000318704, -0.000318704, -5.35693e-08, -3.07939e-05, 5.20865e-06,
                        -7.50718e-06, 4.8216e-07, -1.45982e-05, 1.1188e-05, -6.8775e-06,
                        1.50784e-06, -5.15006e-07, -4.94762e-07, 3.48212e-07, -1.52504e-07,
                        4.78507e-08, -2.51985e-08, 5.53436e-09, -2.04557e-09, 2.15915e-09,
                        -2.16965e-08, 1.11192e-07, -1.14175e-07, 8.70649e-09, -2.22403e-09,
                        9.94639e-10, -3.65263e-10, 8.59842e-11, 1.48633e-10, -2.5425e-10,
                        -1.13839e-10, 2.81633e-10, -4.05259e-10, 2.51296e-10, -1.95286e-10,
                        2.31119e-10, -2.28801e-10, 1.58039e-10, -3.10665e-10, 1.23379e-09,
                        1.04603e-09, -2.40153e-09, 4.8177e-10, -4.00393e-10, -1.78225e-10,
                        -5.67923e-10, -2.25862e-10, -2.25862e-10, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}
