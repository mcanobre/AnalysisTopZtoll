double Wplus_NNPDF30_CT14nnlo(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { 1.31428, 1.11645, 1.00062, 0.980964, 0.979868,
                        0.981741, 0.984141, 0.986953, 0.989344, 0.991172,
                        0.992541, 0.995173, 0.997544, 0.998873, 1,
                        1.00158, 1.00291, 1.00374, 1.0044, 1.00608,
                        1.00693, 1.00782, 1.0091, 1.01007, 1.0127,
                        1.01519, 1.01674, 1.01569, 1.01056, 0.99773,
                        0.975994, 0.941553, 0.892056, 0.824416, 0.735459,
                        0.621858, 0.479528, 0.303539, -0.169735, -0.84898,
                        -1.78921, -3.02538, -4.50542, -5.93079, -6.3149,
                        -3.04024, 10.5457, 51.3315, 165.578 };
   const double fB[49] = { -0.0670195, -0.0338211, -0.00138185, -0.00129613, 0.000341152,
                        0.000164429, 0.000283113, 0.000266827, 0.000210483, 0.000156808,
                        0.000121375, 0.000101103, 7.45296e-05, 4.47349e-05, 4.12988e-05,
                        2.8099e-05, 2.09775e-05, 2.52155e-06, 1.34682e-05, 1.39503e-05,
                        6.6186e-06, 1.17618e-05, 1.15882e-05, 9.17946e-06, 1.08447e-05,
                        8.94011e-06, 1.87933e-06, -1.04284e-05, -3.43869e-05, -6.75833e-05,
                        -0.000110066, -0.000166279, -0.000232068, -0.000311097, -0.000402715,
                        -0.000508736, -0.000633507, -0.000777058, -0.00113402, -0.00160197,
                        -0.00217495, -0.00275662, -0.00309583, -0.0022925, 0.00140897,
                        0.0139999, 0.043755, 0.13721, 0.337598 };
   const double fC[49] = { 0.00487204, 0.00342756, -0.000183636, 0.000192208, -2.84796e-05,
                        1.08073e-05, 1.06116e-06, -2.6897e-06, -2.94471e-06, -2.42279e-06,
                        -1.12052e-06, 3.09619e-07, -1.37255e-06, 1.80758e-07, -3.18199e-07,
                        5.42032e-08, -1.96633e-07, 1.20734e-08, 9.73928e-08, -9.25712e-08,
                        1.92539e-08, 3.21777e-08, -3.39136e-08, 9.8266e-09, -3.16549e-09,
                        -4.45303e-09, -2.37901e-08, -2.54407e-08, -7.03934e-08, -6.23924e-08,
                        -1.07537e-07, -1.17318e-07, -1.45834e-07, -1.70283e-07, -1.96191e-07,
                        -2.27894e-07, -2.71188e-07, -3.03016e-07, -4.10908e-07, -5.25001e-07,
                        -6.20955e-07, -5.42382e-07, -1.36032e-07, 1.74269e-06, 5.66025e-06,
                        1.95215e-05, 3.99888e-05, 0.000146922, 500 };
   const double fD[49] = { -0.000120373, -0.000120373, 1.25281e-05, -7.35625e-06, 1.30956e-06,
                        -3.2487e-07, -1.25029e-07, -8.50037e-09, 1.73973e-08, 4.34091e-08,
                        1.90685e-08, -2.24289e-08, 2.07108e-08, -6.65276e-09, 2.48268e-09,
                        -1.67224e-09, 6.95689e-10, 2.84398e-10, -6.33213e-10, 3.7275e-10,
                        4.30795e-11, -2.20304e-10, 1.45801e-10, -1.73228e-11, -1.71672e-12,
                        -2.57827e-11, -2.20077e-12, -5.99369e-11, 1.0668e-11, -6.01925e-11,
                        -1.30421e-11, -3.80215e-11, -3.2598e-11, -3.45435e-11, -4.22705e-11,
                        -5.77263e-11, -4.24374e-11, -7.19279e-11, -7.60619e-11, -6.39692e-11,
                        5.23817e-11, 2.709e-10, 1.25248e-09, 2.6117e-09, 9.24086e-09,
                        1.36448e-08, 7.12886e-08, 7.12886e-08, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}
