double Wplus_CT14nnlo_EWbun_EV_5(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { -4.12335, -2.13904, -1.23686, -1.14213, -0.805292,
                        -0.797731, -0.704846, -0.486058, -0.599401, -0.428036,
                        -0.440205, -0.402253, -0.319332, -0.225479, -0.0878156,
                        0.0315956, 0.14556, 0.373832, 0.43956, 0.642109,
                        0.692521, 0.71708, 0.817878, 0.832365, 0.967937,
                        1.19586, 1.51869, 1.74368, 1.85254, 1.89179,
                        1.72543, 1.35214, 0.774979, -0.155844, -1.0628,
                        -2.35336, -3.79954, -5.5189, -9.84727, -15.3061,
                        -22.4211, -31.5313, -43.7141, -60.4271, -84.3284,
                        -120.372, -178.003, -276.666, -461.165 };
   const double fB[49] = { 0.685946, 0.327328, -0.0149168, 0.0314102, 0.0187459,
                        -0.00307354, 0.0236821, 0.00184713, 0.000562973, 0.0133074,
                        -0.00603407, 0.00439752, 0.00294883, 0.00501997, 0.00475324,
                        0.00164455, 0.00267111, 0.00120814, 0.00131633, 0.00157486,
                        -2.69521e-05, 0.000782071, 0.000659387, 3.89578e-05, 0.000792237,
                        0.00115406, 0.00120057, 0.000617466, 0.000335725, -0.00018304,
                        -0.00112884, -0.00177741, -0.00316693, -0.00365063, -0.00428393,
                        -0.00558385, -0.00622154, -0.00751643, -0.00969307, -0.0124346,
                        -0.0160114, -0.0208708, -0.0282633, -0.0394508, -0.0576194,
                        -0.0897415, -0.145461, -0.266176, -0.488807 };
   const double fC[49] = { -0.0527458, -0.0369086, 0.00268412, 0.00194857, -0.003215,
                        0.00103305, 0.00164251, -0.00382601, 0.00369759, -0.00242315,
                        0.000488998, -7.1735e-05, 1.37876e-05, 6.90579e-05, -7.97272e-05,
                        1.75535e-05, 2.97776e-06, -1.76075e-05, 1.86894e-05, -1.61041e-05,
                        8.59558e-08, 8.00428e-06, -9.23112e-06, 3.02683e-06, -1.37073e-08,
                        1.46101e-06, -1.27499e-06, -1.05741e-06, -6.9559e-08, -2.0055e-06,
                        -1.77772e-06, -8.16553e-07, -4.74154e-06, 2.80677e-06, -5.33997e-06,
                        1.40283e-07, -2.69102e-06, -2.48857e-06, -1.86471e-06, -3.61841e-06,
                        -3.53514e-06, -6.18369e-06, -8.60129e-06, -1.37737e-05, -2.25634e-05,
                        -4.16807e-05, -6.97581e-05, -0.000171673, 500 };
   const double fD[49] = { 0.00131976, 0.00131976, -2.45184e-05, -0.000172119, 0.000141602,
                        2.03153e-05, -0.000182284, 0.000250787, -0.000204025, 9.70715e-05,
                        -7.47645e-06, 1.1403e-06, 7.36937e-07, -1.9838e-06, 6.48538e-07,
                        -9.71713e-08, -6.86175e-08, 1.20989e-07, -1.15978e-07, 5.39667e-08,
                        2.63944e-08, -5.74513e-08, 4.08598e-08, -4.05404e-09, 1.96629e-09,
                        -3.648e-09, 2.90114e-10, 1.31713e-09, -2.58125e-09, 3.03711e-10,
                        1.28155e-09, -5.23331e-09, 1.00644e-08, -1.08623e-08, 7.30701e-09,
                        -3.77507e-09, 2.69925e-10, 4.15909e-10, -1.16913e-09, 5.55165e-11,
                        -1.7657e-09, -1.61173e-09, -3.4483e-09, -5.85978e-09, -1.27448e-08,
                        -1.87183e-08, -6.79434e-08, -6.79434e-08, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}
