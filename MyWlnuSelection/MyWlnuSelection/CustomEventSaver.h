#ifndef MYWLNUSELECTION_CUSTOMEVENTSAVER_H
#define MYWLNUSELECTION_CUSTOMEVENTSAVER_H

#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "LPXKfactorTool/LPXKfactorTool.h"
#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TopEventSelectionTools/PlotManager.h"

/**
 * This class shows you how to extend the flat ntuple to include your own variables
 * 
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work 
 * 
 */

namespace top{
    class CustomEventSaver : public top::EventSaverFlatNtuple {
        public:
            ///-- Default constrcutor with no arguments - needed for ROOT --///
            CustomEventSaver();
            ///-- Destructor does nothing --///
            virtual ~CustomEventSaver(){}

            ///-- initialize function for top::EventSaverFlatNtuple --///
            ///-- We will be setting up out custom variables here --///
            virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;

            ///-- Keep the asg::AsgTool happy --///
            virtual StatusCode initialize(){return StatusCode::SUCCESS;}      

            ///-- saveEvent function for top::EventSaverFlatNtuple --///
            ///-- We will be setting our custom variables on a per-event basis --///
            virtual void saveEvent(const top::Event& event) override;
            virtual void saveTruthEvent();

        private:
            ///-- Some additional custom variables for the output --///
            //float m_randomNumber;
            //float m_someOtherVariable;
            ///-- Hadronic Recoil variables --///
            //TLorentzVector m_PFOHR;
            //double m_sumET_PFOHR;
            //hadrecoil *m_hadrecoil;

            // Tool for tag event if electron is tight
            AsgElectronLikelihoodTool *m_ElTightLH;
            bool m_isElTight;

            LPXKfactorTool *m_kfactorTool;
            double m_weight_kfactor;
            double m_weight_kfactor_truth;
	    double m_BornMass;
	    float m_weight_pileup_hash_truth;
	    float m_weight_pileup_hash;

    	    std::vector<int> m_mc_status;
    	    std::vector<int> m_mc_barcode;
    	    float m_mc_Lepton_Pt;
	    float m_mc_AntiLepton_Pt;
	    float m_mc_Lepton_Phi;
	    float m_mc_AntiLepton_Phi;
	    float m_mc_Lepton_Eta;
	    float m_mc_AntiLepton_Eta;
	    float m_mc_Lepton_m;
	    float m_mc_AntiLepton_m;
	    float m_mc_Lepton_Pt_bare;
	    float m_mc_AntiLepton_Pt_bare;
	    float m_mc_Lepton_Phi_bare;
	    float m_mc_AntiLepton_Phi_bare;
	    float m_mc_Lepton_Eta_bare;
	    float m_mc_AntiLepton_Eta_bare;
	    float m_mc_Lepton_m_bare;
	    float m_mc_AntiLepton_m_bare; 

	    std::shared_ptr<top::TreeManager> m_truthTreeManager;
  	    std::shared_ptr<top::TopConfig> m_config;

            ///-- Tell RootCore to build a dictionary (we need this) --///
            ClassDef(top::CustomEventSaver, 0);
    };
}

#endif
