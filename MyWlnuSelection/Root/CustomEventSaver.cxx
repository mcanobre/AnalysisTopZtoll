#include "MyWlnuSelection/CustomEventSaver.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/TreeManager.h"

#include <TRandom3.h>

#include <exception>

//#include "TopEvent/Event.h"
//#include "TopEvent/EventTools.h"
//#include "TopEventSelectionTools/TreeManager.h"
//#include "TopParticleLevel/ParticleLevelEvent.h"
#include "TopConfiguration/TopConfig.h"
//#include "TopEventSelectionTools/PlotManager.h"

//#include "TopEvent/TopEventMaker.h"
//#include "TopEvent/Event.h"

#include "TH1.h"
#include "TH2.h"

namespace top {
    ///-- Constrcutor --///
    CustomEventSaver::CustomEventSaver() :
        m_isElTight(false),
        m_weight_kfactor(1.0),
        m_weight_kfactor_truth(1.0),
        m_BornMass(0.0),
    	m_mc_Lepton_Pt(0.0),
        m_weight_pileup_hash(0),
        m_weight_pileup_hash_truth(0),
	m_mc_AntiLepton_Pt(0.0),
	m_mc_Lepton_Phi(0.0),
	m_mc_AntiLepton_Phi(0.0),
	m_mc_Lepton_Eta(0.0),
	m_mc_AntiLepton_Eta(0.0),
	m_mc_Lepton_m(0.0),
	m_mc_AntiLepton_m(0.0),
	m_mc_Lepton_Pt_bare(0.0),
	m_mc_AntiLepton_Pt_bare(0.0),
	m_mc_Lepton_Phi_bare(0.0),
	m_mc_AntiLepton_Phi_bare(0.0),
	m_mc_Lepton_Eta_bare(0.0),
	m_mc_AntiLepton_Eta_bare(0.0),
	m_mc_Lepton_m_bare(0.0),
	m_mc_AntiLepton_m_bare(0.0),
  	m_truthTreeManager(nullptr)
    {
    }

    ///-- initialize - done once at the start of a job before the loop over events --///
    void CustomEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
    {
        ///-- Let the base class do all the hard work --///
        ///-- It will setup TTrees for each systematic with a standard set of variables --///
        m_config = config;
        top::EventSaverFlatNtuple::initialize(config, file, extraBranches);

        if (m_config->isMC()){
   	    m_truthTreeManager = EventSaverFlatNtuple::truthTreeManager();
            /*m_truthTreeManager->makeOutputVariable(m_mc_status, "mc_status");
            m_truthTreeManager->makeOutputVariable(m_mc_barcode, "mc_barcode");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Pt, "Lepton_Pt");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Pt, "AntiLepton_Pt");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Eta, "Lepton_Eta");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Eta, "AntiLepton_Eta");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Phi, "Lepton_Phi");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Phi, "AntiLepton_Phi");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_m, "Lepton_m");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_m, "AntiLepton_m");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Pt, "Lepton_Pt_bare");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Pt, "AntiLepton_Pt_bare");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Eta, "Lepton_Eta_bare");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Eta, "AntiLepton_Eta_bare");
            m_truthTreeManager->makeOutputVariable(m_mc_Lepton_Phi, "Lepton_Phi_bare");
            m_truthTreeManager->makeOutputVariable(m_mc_AntiLepton_Phi, "AntiLepton_Phi_bare");
	    m_truthTreeManager->makeOutputVariable(m_weight_pileup_hash_truth, "weight_pileup_hash_truth");
            m_truthTreeManager->makeOutputVariable(m_weight_kfactor_truth, "weight_KFactor_truth");*/
  	} // end of if statement 

        // Initialize AsgElectronLikelihoodTool
        m_ElTightLH = new AsgElectronLikelihoodTool("TightLH");
        top::check(m_ElTightLH->setProperty("WorkingPoint", "TightLHElectron"),
                "Failed to set config for AsgElectronLikelihoodTool");
        top::check(m_ElTightLH->initialize(),
                "Failed to initialize AsgElectronLikelihoodTool");

        // K factor
        m_kfactorTool = new LPXKfactorTool("LPXKfactorTool");
        top::check(m_kfactorTool->setProperty("isMC15",true),
                "Failed to set property for k-factor calculation");
        top::check(m_kfactorTool->initialize(),
                "Failed initialize k factor calculation");

        ///-- Loop over the systematic TTrees and add the custom variables --///
        for (auto systematicTree : treeManagers()) {
            //systematicTree->makeOutputVariable(m_randomNumber, "randomNumber");
            //systematicTree->makeOutputVariable(m_someOtherVariable,"someOtherVariable");
            systematicTree->makeOutputVariable(m_isElTight, "el_isTight");
            //systematicTree->makeOutputVariable(m_PFOHR, "PFOHR");
            //systematicTree->makeOutputVariable(m_sumET_PFOHR, "sumET_PFOHR");
            systematicTree->makeOutputVariable(m_weight_kfactor, "weight_KFactor");
	    systematicTree->makeOutputVariable(m_BornMass, "BornMass");
	    systematicTree->makeOutputVariable(m_weight_pileup_hash, "weight_pileup_hash");
            systematicTree->makeOutputVariable(m_mc_Lepton_Pt, "Lepton_Pt");
            systematicTree->makeOutputVariable(m_mc_AntiLepton_Pt, "AntiLepton_Pt");
            systematicTree->makeOutputVariable(m_mc_Lepton_Eta, "Lepton_Eta");
            systematicTree->makeOutputVariable(m_mc_AntiLepton_Eta, "AntiLepton_Eta");
            systematicTree->makeOutputVariable(m_mc_Lepton_Phi, "Lepton_Phi");
            systematicTree->makeOutputVariable(m_mc_AntiLepton_Phi, "AntiLepton_Phi");
            systematicTree->makeOutputVariable(m_mc_Lepton_m, "Lepton_m");
            systematicTree->makeOutputVariable(m_mc_AntiLepton_m, "AntiLepton_m");
            systematicTree->makeOutputVariable(m_mc_Lepton_Pt, "Lepton_Pt_bare");
            systematicTree->makeOutputVariable(m_mc_AntiLepton_Pt, "AntiLepton_Pt_bare");
            systematicTree->makeOutputVariable(m_mc_Lepton_Eta, "Lepton_Eta_bare");
            systematicTree->makeOutputVariable(m_mc_AntiLepton_Eta, "AntiLepton_Eta_bare");
            systematicTree->makeOutputVariable(m_mc_Lepton_Phi, "Lepton_Phi_bare");
            systematicTree->makeOutputVariable(m_mc_AntiLepton_Phi, "AntiLepton_Phi_bare");

        }
    }

    ///-- saveEvent - run for every systematic and every event --///
    void CustomEventSaver::saveEvent(const top::Event& event) 
    {
        ///-- set our variables to zero --///
        //m_randomNumber = 0.;
        //m_someOtherVariable = 0.;

        // Apply ID selection
        for (const auto elItr : event.m_electrons) {
            if (elItr->pt() >= 20000) {
                if (m_ElTightLH->accept(elItr)) m_isElTight = true;
            }
        }

        if (event.m_info->eventType(xAOD::EventInfo::IS_SIMULATION)) {

	   if (event.m_info->isAvailable<unsigned long long>("PileupWeight_Hash"))
      m_weight_pileup_hash = event.m_info->auxdataConst<unsigned long long>("PileupWeight_Hash");
	  top::check(m_kfactorTool->getKFactor(m_weight_kfactor), "Error getting LPXKfactor.");
	  //m_BornMass = event.m_info->auxdata<double>("BornMass");
        }

        ///-- Let the base class do all the hard work --///
        top::EventSaverFlatNtuple::saveEvent(event);
    } // end of saveEvent

    ///-- saveTruthEvent - store some extra stuff from the truth information --///
    void CustomEventSaver::saveTruthEvent() 
    {

        if( m_config->isMC() && m_config->useTruthParticles() /*&& m_config->doTruthBlockInfo()*/ ) {

  		const xAOD::EventInfo* eventInfo(nullptr);
  		top::check( evtStore()->retrieve(eventInfo, m_config->sgKeyEventInfo()) , "Failed to retrieve EventInfo" );

	        const xAOD::TruthParticleContainer* truth(nullptr);
	        top::check( evtStore()->retrieve(truth, m_config->sgKeyMCParticle()) , "Failed to retrieve TruthParticleContainer" );

	        //if (event.m_info->isAvailable<unsigned long long>("PileupWeight_Hash"))
                m_weight_pileup_hash_truth = eventInfo->auxdataConst<unsigned long long>("PileupWeight_Hash");

	        top::check(m_kfactorTool->getKFactor(m_weight_kfactor_truth), "Error getting LPXKfactor.");
		bool FoundLepton=false, FoundAntiLepton=false, FoundBareLepton=false, FoundBareAntiLepton=false;
		TLorentzVector Lepton(0,0,0,0),AntiLepton(0,0,0,0), Propagator(0,0,0,0), ZBoson(0,0,0,0);
                for (const auto* const mcPtr : *truth) {

	          m_mc_status.clear();
        	  m_mc_barcode.clear();

         	  if (truth != nullptr) {
	            unsigned int i(0);
	            unsigned int truthSize = truth->size();
	            //m_mc_status.resize(truthSize);
        	    //m_mc_barcode.resize(truthSize);
                    //m_mc_barcode[i] = mcPtr->barcode();
                    //m_mc_status[i] = mcPtr->status();
		    
		    if( mcPtr->pdgId() == 23 ){
			//std::cout<<"Found ZBoson with status "<<mcPtr->status()<<" and mass "<<0.001*(mcPtr->p4()).Mag()<<std::endl;
			ZBoson = mcPtr->p4();
		    } // end of if statement

		    // Getting lepton information for Powheg
		    if( fabs(mcPtr->pdgId()) == 11 || fabs(mcPtr->pdgId()) == 13 ){

			if( mcPtr->status() == 3 ){
				if( mcPtr->pdgId() == 11 || mcPtr->pdgId() == 13 && !FoundLepton ){ 

					FoundLepton=true;
					m_mc_Lepton_Pt = mcPtr->pt();
					m_mc_Lepton_Eta = mcPtr->eta();
					m_mc_Lepton_Phi = mcPtr->phi();
					m_mc_Lepton_m = (mcPtr->p4()).M();
					Lepton.SetPtEtaPhiM( 0.001*m_mc_Lepton_Pt, m_mc_Lepton_Eta, m_mc_Lepton_Phi, 0.001*m_mc_Lepton_m );
			        } // end of if lepton 				

				if( mcPtr->pdgId() == -11 || mcPtr->pdgId() == -13 && !FoundAntiLepton ){ 
					//std::cout<<"Found antilepton with pT "<<0.001*mcPtr->pt()<<std::endl;
					FoundAntiLepton=true;
					m_mc_AntiLepton_Pt = mcPtr->pt();
					m_mc_AntiLepton_Eta = mcPtr->eta();
					m_mc_AntiLepton_Phi = mcPtr->phi();
					m_mc_AntiLepton_m = (mcPtr->p4()).M();
					AntiLepton.SetPtEtaPhiM( 0.001*m_mc_AntiLepton_Pt, m_mc_AntiLepton_Eta, m_mc_AntiLepton_Phi, 0.001*m_mc_AntiLepton_m );
			        } // end of if lepton 
				if( FoundLepton && FoundAntiLepton ){ 
					Propagator=Lepton+AntiLepton;
					//std::cout<<"Dilepton Mass "<<Propagator.Mag()<<std::endl;
				} // end of lepton
			} // end of if statement
			// if Bare lepton
			if( mcPtr->status() == 1 ){

				if( mcPtr->pdgId() == 11 || mcPtr->pdgId() == 13 && !FoundBareLepton ){ 
					FoundBareLepton=true;
					m_mc_Lepton_Pt_bare = mcPtr->pt();
					m_mc_Lepton_Eta_bare = mcPtr->eta();
					m_mc_Lepton_Phi_bare = mcPtr->phi();
					m_mc_Lepton_m_bare = mcPtr->m();

			        } // end of if lepton 				

				if( mcPtr->pdgId() == -11 || mcPtr->pdgId() == -13 && !FoundBareAntiLepton ){ 
					FoundBareAntiLepton=true;
					m_mc_AntiLepton_Pt_bare = mcPtr->pt();
					m_mc_AntiLepton_Eta_bare = mcPtr->eta();
					m_mc_AntiLepton_Phi_bare = mcPtr->phi();
					m_mc_AntiLepton_m_bare = mcPtr->m();

			        } // end of if lepton 
				if( FoundBareLepton && FoundBareAntiLepton ){ 
					//Propagator=Lepton+AntiLepton;
					//std::cout<<"Bare Lepton pT "<<0.001*m_mc_Lepton_Pt_bare<<std::endl;
					//std::cout<<"Bare AntiLepton pT "<<0.001*m_mc_AntiLepton_Pt_bare<<std::endl;
				} // end of lepton
			} // end of if statement

		    } // end of if statement
                    ++i;
                  } // end of if not null pointer
                } // end of particle Loop
    		EventSaverFlatNtuple::saveTruthEvent();
	} // end of isSimulation */
  } // end of saveTruthEvent

} // end of program
