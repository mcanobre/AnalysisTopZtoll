#!/usr/bin/env python
"""
Simple tool for sum histograms.
"""
import sys
import ROOT

if len(sys.argv) < 3:
	sys.exit("Usage: printCutflow.py [hist Name] [file list] ...\n")

hFile = []
hName = sys.argv[1]
files = open(sys.argv[2], "r")
fName1 = files.readline()
fName1 = fName1[:-1]
sys.stdout.write("openning"+fName1+"\n")
hFile.append(ROOT.TFile(fName1, "r"))
if hFile[0].IsZombie(): sys.exit("Error openning file.\n")
hAux = hFile[0].Get(hName)
h = hAux
c1 = ROOT.TCanvas("c1", "c1", 800, 600)
c1.SetLogy()
#hAux.Draw("HIST")
#hAux.SetStats(0)
#hAux.GetXaxis().SetTitle("m_{W} [GeV]")
#hAux.Draw("HIST")

while True:
	fName = files.readline()
	fName = fName[:-1]
	if fName == "": break
	sys.stdout.write("openning "+fName+"\n")
	hFile.append(ROOT.TFile(fName, "r"))
	n = len(hFile)
	hAux = hFile[n - 1].Get(hName)
	hAux.SetLineColor(1 + (n % 4))
	hAux.SetStats(0)
	#hAux.Draw("SAME")
	h.Add(hAux)
	hFile[n - 1].Close()

h.SetStats(0)
h.Draw("HIST *P")
#c1.Print("Cutflow.png")
files.close()
outFile = ROOT.TFile("hCutflow.root", "recreate")
h.Write()
c1.Write()
outFile.Write()
outFile.Close()
hFile[0].Close()

#for f in hFile: f.Close()
